%{
  #include <stdio.h>

  void yyerror(const char * str) {
    /* 
      str is the error message passed by YACC.
      We'll want to reformat this at some point.
    */
    fprintf(stderr, "error: %s\n", str);
  }

  int yywrap(void) {
    /* This is where passing to user control should go */
    return 1;
  }

  main() {
    yyparse();
  }

%}

%union
{
  double number;
  int num_op;
  char letter_op;
  char * string;
}

%token EOF NEWLINE 
%token <string> STRING 
%token <letter_op> LETTER 
%token <number> NUMBER
%token <letter_op> MUL_OP
%token <num_op> ASSIGN_OP
%token <num_op> REL_OP
%token <num_op> INCR_DECR
%token DEFINE BREAK QUIT LENGTH
%token RETURN FOR IF WHILE SQRT
%token SCALE IBASE OBASE AUTO
%token OPAREN CPAREN OSQR CSQR OCURL CCURL
%token COMMA SEMICOLON
%token PLUS MINUS CARET

%start program

%%

program         : EOF
                | input_item program
                ;

input_item      : semicolon_list NEWLINE
                | function
                ;

semicolon_list  : /* empty */
                | statement
                | semicolon_list SEMICOLON statement
                | semicolon_list SEMICOLON
                ;

statement       : expression
                | STRING
                | BREAK
                | QUIT
                | RETURN
                | RETURN OPAREN return_expression CPAREN
                | FOR OPAREN expression SEMICOLON
                          relational_expression SEMICOLON
                          expression CPAREN statement
                | IF OPAREN relational_expression CPAREN statement
                | WHILE OPAREN relational_expression CPAREN statement
                | OCURL statement_list CCURL
                ;

function        : DEFINE LETTER OPAREN opt_parameter_list CPAREN
                        OCURL NEWLINE opt_auto_define_list
                        statement_list CCURL
                ;

opt_parameter_list    : /* empty */
                      | parameter_list
                      ;

parameter_list  : LETTER
                | define_list COMMA LETTER

opt_auto_define_list  : /* empty */
                      | AUTO define_list NEWLINE
                      | AUTO define_list SEMICOLON
                      ;

define_list     : LETTER
                | LETTER OSQR CSQR
                | define_list COMMA LETTER
                | define_list COMMA LETTER OSQR CSQR
                ;

opt_argument_list     : /* empty */
                      | argument_list
                      ;

argument_list   : expression
                | LETTER OSQR CSQR COMMA argument_list
                ;

relational_expression : expression
                      | expression REL_OP expression
                      ;

return_expression     : /* empty */
                      | expression
                      ;

expression      : named_expression
                | NUMBER
                | OPAREN expression CPAREN
                | LETTER OPAREN opt_argument_list CPAREN
                | MINUS expression
                | expression PLUS expression
                | expression MINUS expression
                | expression MUL_OP expression
                | expression CARET expression
                | INCR_DECR named_expression
                | named_expression INCR_DECR
                | named_expression ASSIGN_OP expression
                | LENGTH OPAREN expression CPAREN
                | SQRT OPAREN expression CPAREN
                | SCALE OPAREN expression CPAREN
                ;

named_expression      : LETTER
                      | LETTER OSQR expression CSQR
                      | SCALE
                      | IBASE
                      | OBASE
                      ;