%{
  #include <string.h>
  #include "y.tab.h"
%}

%%

"/*"(\.)*"*/"             ;
\n                        return NEWLINE;
\".\"                     yylval.string=strdup(yytext); return STRING;
[ \t]                     ;
\\\n                      ;
[0-9A-F]*(\.)?[0-9A-F]*   yylval.number=atof(yytext); return NUMBER;
auto                      return AUTO;
break                     return BREAK;
define                    return DEFINE;
ibase                     return IBASE;
if                        return IF;
for                       return FOR;
length                    return LENGTH;
obase                     return OBASE;
quit                      return QUIT;
return                    return RETURN;
scale                     return SCALE;
sqrt                      return SQRT;
while                     return WHILE;
[a-z]                     yylval.letter_op=yytext[0]; return LETTER
[-+*/%^]?=                return ASSIGN_OP;
=-                        ;
[*/%]                     yylval.letter_op=yytext[0]; return MUL_OP;
([=<>]?(=)?)|!=           return REL_OP;
\+\+|--                   yylval.num_op=strcmp(yytext, "++"); return INCR_DECR;
"("                       return OPAREN;
")"                       return CPAREN;
,                         return COMMA;
"+"                       return PLUS;
-                         return MINUS;
;                         return SEMICOLON;
"["                       return OSQR;
"]"                       return CSQR;
"^"                       return CARET;
"{"                       return OCURL;
"}"                       return CCURL;
<<EOF>>                   return EOF;

%%