#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
  int rest_time = atoi(argv[1]);
  sleep(rest_time);
  return 0;
}