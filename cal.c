#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

typedef enum months{Jan = 0, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec} months;
int month_key[12] = {1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6};
char * prog_name;

int is_lyear(int);
int get_first_day(int, months);
int get_last_day(int, months);
char * get_month_string(months);
int stoi_mini(char *);

int main(int argc, char * argv[]) {
  /*
    printf("\033[30;47mBlack on White\033[0m\n");
    printf("\033[37;40mWhite on Black\033[0m\n");
  */
  prog_name = argv[0];

  

  int calendar[7][6];
  months month;
  int year;
  int cur_day = 0;

  if(argc == 1) {
    time_t t = time(0);
    struct tm* cur_date = localtime(&t);
    month = cur_date->tm_mon;
    /* 
       Note to future me:
       Years are given relative to 1900.
       That's why it's tm_year + 1900.
       Have a nice day! ;D
       -Kat
    */
    year = cur_date->tm_year + 1900;
    cur_day = cur_date->tm_mday;
  }
  else if(argc == 2) {
    fprintf(stderr, "%s: feature currently not implemented\n", prog_name);
    exit(127);
  }
  else if(argc == 3) {
    month = stoi_mini(argv[1]) - 1;
    year = stoi_mini(argv[2]);
  }

  if(year < 1) {
    fprintf(stderr, "%s: invalid year: use positive integer\n", prog_name);
    exit(1);
  }

  char * month_string = get_month_string(month);

  int d = 1;
  int i, j;
  for(i = 0; i < 6; i++) {
    if(i == 0) {
      for(j = 0; j < get_first_day(year, month); j++) {
	calendar[j][i] = 0;
      }
    }
    for(j = (i == 0) ? get_first_day(year, month) : 0; j < 7; j++) {
      calendar[j][i] = d;
      d++;
    }
  }

  printf("%s %d\n", month_string, year);
  printf("Su Mo Tu We Th Fr Sa\n");
  for(i = 0; i < 6; i++) {
    for(j = 0; j < 7; j++) {
      if(calendar[j][i] == 0) {
	printf("   ");
      }
      else if(calendar[j][i] > get_last_day(year, month)) {
	i = 6; j = 7;
	break;
      }
      else {
	if(calendar[j][i] == cur_day && cur_day != 0) {
	  if(isatty(fileno(stdout))) {
	    printf("\033[7m%2d\033[0m ", calendar[j][i]);
	  }
	  else {
	    printf("%2d ", calendar[j][i]);
	  }
	}
	else {
	  printf("%2d ", calendar[j][i]);
	}
      }
    }
    printf("\n");
  }
}

/* 
   l for leap!
   -Kat
 */
int is_lyear(int year) {
  if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
    return 1;
  }
  else {
    return 0;
  }
}

int get_first_day(int year, months month) {
  int calc = year % 100;
  calc /= 4;
  calc += 1;
  calc += month_key[month];
  if(is_lyear(year)) {
    calc -= 1;
  }
  if(year > 1752) {
    int temp = year / 100;
    calc += (temp == 19) ? 0 :
      (temp == 20) ? 6 :
      (temp == 17) ? 4 : 2;
  }
  else {
    int temp = year / 100;
    calc += (18 - temp);
  }
  calc += (year % 100);
  calc %= 7;
  calc -= 1;
  return calc;
}

char * get_month_string(months month) {
  if(month == Jan) {
    return "January";
  }
  else if(month == Feb) {
    return "February";
  }
  else if(month == Mar) {
    return "March";
  }
  else if(month == Apr) {
    return "April";
  }
  else if(month == May) {
    return "May";
  }
  else if(month == Jun) {
    return "June";
  }
  else if(month == Jul) {
    return "July";
  }
  else if(month == Aug) {
    return "August";
  }
  else if(month == Sep) {
    return "September";
  }
  else if(month == Oct) {
    return "October";
  }
  else if(month == Nov) {
    return "November";
  }
  else if(month == Dec) {
    return "December";
  }
  else {
    fprintf(stderr, "%s: invalid month passed in: use 1-12\n", prog_name);
    exit(1);
  }
}


int get_last_day(int year, months month) {
  if(month == Jan) {
    return 31;
  }
  else if(month == Feb) {
    return is_lyear(year) ? 29 : 28;
  }
  else if(month == Mar) {
    return 31;
  }
  else if(month == Apr) {
    return 30;
  }
  else if(month == May) {
    return 31;
  }
  else if(month == Jun) {
    return 30;
  }
  else if(month == Jul) {
    return 31;
  }
  else if(month == Aug) {
    return 31;
  }
  else if(month == Sep) {
    return 30;
  }
  else if(month == Oct) {
    return 31;
  }
  else if(month == Nov) {
    return 30;
  }
  else if(month == Dec) {
    return 31;
  }
}

int stoi_mini(char * str) {
  int i, str_int = 0;

  for(i = 0; str[i] != '\0' && str[i] != ','; i++) {
    str_int *= 10;
    switch(str[i]) {
      case '0': str_int += 0; break;
      case '1': str_int += 1; break;
      case '2': str_int += 2; break;
      case '3': str_int += 3; break;
      case '4': str_int += 4; break;
      case '5': str_int += 5; break;
      case '6': str_int += 6; break;
      case '7': str_int += 7; break;
      case '8': str_int += 8; break;
      case '9': str_int += 9; break;
      default: return -1;
    }
  }

  return str_int;
}
