#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

int main(int argc, char ** argv) {
  char ** util_argv;

  if(signal(SIGHUP, SIG_IGN) == SIG_ERR) {
    return 127;
  }

  util_argv = calloc(argc, sizeof(*argv));
  if(util_argv == NULL) {
    return 127;
  }

  for(int i = 0; i < argc - 1; i++) {
    util_argv[i] = calloc(strlen(argv[i+1]) + 1, sizeof(*(util_argv[i])));
    strcpy(util_argv[i], argv[i+1]);
  }
  util_argv[argc-1] = NULL;

  if(execvp(util_argv[0], util_argv) < 0) {
    return 126;
  }
}