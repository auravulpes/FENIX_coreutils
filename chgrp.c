#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int chgrp_dir(char *, int);

int main(int argc, char * argv[]) {
  char c;
  int recurse = 0; /* -R */
  /* 0: -P, 01: -L, 02: -H */
  int recurse_follow = 0;
  int no_follow_link = 0; /* -h */
  gid_t group;

  while((c = getopt(argc, argv, "RHLPh")) != -1) {
    switch(c) {
      case 'R': recurse = 1; break;
      case 'P': recurse_follow = 0; break;
      case 'L': recurse_follow = 01; break;
      case 'H': recurse_follow = 02; break;
      case 'h': no_follow_link = 1; break;
    }
  }

  group = atoi(argv[optind++]);

  for(; optind < argc; optind++) {
    if(/* is a directory && */ recurse != 0) {
      chgrp_dir(argv[optind], recurse_follow);
    }
    else if(no_follow_link != 0) {
      chown(argv[optind], -1, group);
    }
    else {
      chown(argv[optind], -1, group);
    }
  }
}

int chgrp_dir(char * path, int recurse_mode) {

}