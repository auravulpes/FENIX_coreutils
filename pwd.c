#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
  /* 
    The -L option prints the logical path, corresponding to the PWD envar.
    -P prints the physical path. The logical path includes any symbolic
    links followed to get there, where as the physical path doesn't. So,
    for example, if /lib links to /usr/lib, then if we cd /lib, pwd -L will
    print /lib, whereas pwd -P will print /usr/lib, even though cd .. will
    take us back to /
  */
  int mode = 01; /* 01: -L, 02: -P */
  char c;

  while((c = getopt(argc, argv, "LP")) != -1) {
    switch(c) {
      case 'L': mode = 01; break;
      case 'P': mode = 02; break;
    }
  }

  if(mode == 01) {
    printf("%s\n", getenv("PWD"));
  }
  else {
    printf("%s\n", getcwd(NULL, 0));
  }

  return 0;
}
