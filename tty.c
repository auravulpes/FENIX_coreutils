#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <string.h>
#include <stdio.h>

int main() {
  /* Check if we're in a tty and, if so, print the devfile for it. */
  if(isatty(fileno(stdin))) {
    printf("%s\n", ttyname(0));
    return 0;
  }
  else {
    printf("not a tty\n");
    return 1;
  }
}
