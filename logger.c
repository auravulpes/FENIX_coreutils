#include <stdio.h>

int main(int argc, char ** argv) {
  if(argc == 1) {
    return 1;
  }

  int i = 1;
  FILE * log = fopen("/var/log/messages", "a+");
  if(log == NULL) {
    return 1;
  }

  fprintf(log, "\n");
  for(i = 1; i < argc - 1; i++) {
    fprintf(log, "%s ", argv[i]);
  }
  fprintf(log, "%s", argv[i]);

  fclose(log);
  return 0;
}