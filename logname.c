#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char * argv[]) {  
  char * logname = getlogin(); /* get current user's name */

  /* If we didn't get their name, find out why. */
  if(logname == NULL) {
    switch(errno) {
      case EMFILE: fprintf(stderr, "%s: all file descriptors open\n", argv[0]);
                   break;
      case ENFILE: fprintf(stderr, "%s: too many files open", argv[0]); break;
      default: fprintf(stderr, "%s: other/unknown error", argv[0]);
    }
    return 1;
  }
  else {
    printf("%s\n", logname); /* Print their name */
    return 0;
  }
}