#define _XOPEN_SOUCE 700

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
  /*
    link(3) takes 2 arguments, no more, no less.
  */
  if(argc < 3) {
    fprintf(stderr, "%s: missing file operand\n", argv[0]);
    exit(1);
  }

  if(argc > 3) {
    fprintf(stderr, "%s: extra operand specified\n", argv[0]);
    exit(1);
  }

  /* Yep, this just calls link(3) */
  return link(argv[1], argv[2]);
}
