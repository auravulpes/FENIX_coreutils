#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv) {
  FILE *in_file;
  int i, error = 0, eof_reached = 0;
  char c, c_temp;

  for(i = 1; i < argc; i++) {
    /* If - is passed as an argument, set the input file to stdin */
    if(argv[i][0] == '-' && argv[i][1] == '\0') {
      in_file = stdin;
    }
    else {
      /* Open the file given */
      in_file = fopen(argv[i], "r");
      if(in_file == NULL) {
        /* If it can't open, set that we encountered an error */
        /* 
          TODO: this should probably check errno instead of 
          assuming that we can't find the file 
        */
        fprintf(stderr, "Cannot find file %s\n", argv[i]);
        error = 1;
        continue;
      }
    }
    /*
      Loop through the file line by line, applying transformations based on
      the first character of the line, as given by the POSIX spec.
    */
    eof_reached = 0;
    while(!eof_reached) {
      c = fgetc(in_file);
      /*
        Get the first character of the line and print the right thing
      */
      switch(c) {
        case '1': printf("\f"); break; /* Form feed on 1 */
        case '0': printf("\n"); break; /* New line on 0 */
        case '+': printf("\r"); break; /* Carriage return on + */
        case EOF: eof_reached = 1; continue; /* Leave on EOF */
        /* Anything else, don't do anything */
      }
      /* Print out the rest of the line unless we hit an EOF, where we quit. */
      for(c_temp = fgetc(in_file); c_temp != '\n'; c_temp = fgetc(in_file)) {
        if(c_temp == EOF) {
          eof_reached = 1;
          break;
        }
        else
          printf("%c", c_temp);
      }
    }
    printf("\n");
  }

  /* If we ever hit an error, return 1. */
  if(error) {
    return 1;
  }
  else {
    return 0;
  }
}
