CFLAGS += -std=c99 -pedantic -g
builddir = bin
prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
mandir = $(prefix)/man
INSTALLPROGRAM = \install

UTILS = asa basename cat cksum cmp dirname echo expand false head link ln \
				logger logname mkfifo pwd rmdir sleep tee true tty uname wc

all: $(UTILS)

install-strip: INSTALLPROGRAM = \install -s
install-strip: install

install: installdirs
	$(INSTALLPROGRAM) $(builddir)/* $(bindir)
	$(INSTALLPROGRAM) *.1 $(mandir)

%: %.c bindirs
	$(CC) $(CFLAGS) -o $(builddir)/$@ $<

installdirs:
	if [ ! -d "$(bindir)"]; then \
		mkdir $(bindir); \
	fi

bindirs:
	if [ ! -d "$(builddir)" ]; then \
		mkdir $(builddir); \
	fi

uninstall:
	#remove utilities in $(bindir)

clean:
	-rm -rf $(builddir)
